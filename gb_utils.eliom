open Printf
open Conll
open Dep2pictlib
open Grewlib

module String_map = Map.Make (String)

(* ================================================================================ *)
exception Error of string


(* ================================================================================ *)
(* global variables *)
(* ================================================================================ *)
let (global : string String_map.t ref) = ref String_map.empty
let set_global key value = global := String_map.add key value !global
let get_global key =
  try String_map.find key !global
  with Not_found -> raise (Error (sprintf "Config error: global parameter `%s` is not set" key))



let uid () = Unix.gettimeofday () *. 10000. |> int_of_float |> string_of_int

let (warnings: Yojson.Basic.t list ref) = ref []
let warn s = warnings := (`String s) :: !warnings

let wrap fct last_arg =
  warnings := [];
  let json =
    try
      let data = fct last_arg in
      match !warnings with
      | [] -> `Assoc [ ("status", `String "OK"); ("data", data) ]
      | l -> `Assoc [ ("status", `String "WARNING"); ("messages", `List l); ("data", data) ]
    with
    | Error msg -> `Assoc [ ("status", `String "ERROR"); ("message", `String msg) ]
    | Conll_error t -> `Assoc [ ("status", `String "ERROR"); ("message", t) ]
    | Dep2pictlib.Error t -> `Assoc [ ("status", `String "ERROR"); ("message", t) ]
    | Grewlib.Error t -> `Assoc [ ("status", `String "ERROR"); ("message", `String t) ]
    | Yojson.Json_error t -> `Assoc [ ("status", `String "ERROR"); ("message", `String t) ]
    | exc -> `Assoc [ ("status", `String "BUG"); ("exception", `String (Printexc.to_string exc)) ] in
  json

(** split the path given when uploading a folder: first level is removed
  "d1/d2/d3/f" --> ("d2/d3", "f")
  *)
let split_path path =
  let rec loop p =
    match (Filename.dirname p, Filename.basename p) with
    | (".", d) -> ""
    | (p1, last) -> Filename.concat (loop p1) last in
  (loop (Filename.dirname path), Filename.basename path)

module Log = struct
  let out_ch = ref stdout

  let time_stamp () =
    let gm = Unix.localtime (Unix.time ()) in
    Printf.sprintf "%02d_%02d_%02d_%02d_%02d_%02d"
      (gm.Unix.tm_year - 100)
      (gm.Unix.tm_mon + 1)
      gm.Unix.tm_mday
      gm.Unix.tm_hour
      gm.Unix.tm_min
      gm.Unix.tm_sec

  let init () =
    let basename = Printf.sprintf "grew_web_back_%s.log" (time_stamp ()) in
    let filename = Filename.concat (get_global "log") basename in
    out_ch := open_out filename

  let _info s = Printf.fprintf !out_ch "[%s] %s\n%!" (time_stamp ()) s
  let info s = Printf.ksprintf _info s
end
