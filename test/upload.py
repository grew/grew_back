import requests
import webbrowser
import sys
import json
import tempfile

if len (sys.argv) > 1 and sys.argv[1] == "prod":
      back = "http://back.grew.fr/"
      front = "http://transform.grew.fr"
else:
      back = "http://localhost:8080/"
      front = "http://localhost:8888/grew_web"


conll = """# sent_id = fr-ud-dev_00243
# text = Ils sont sept.
1	Ils	il	PRON	_	Gender=Masc|Number=Plur|Person=3|PronType=Prs	2	subj	_	wordform=ils
2	sont	être	AUX	_	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
3	sept	sept	NUM	_	Number=Plur	2	comp:pred	_	SpaceAfter=No
4	.	.	PUNCT	_	_	2	punct	_	_

"""

grs = """
rule r { pattern { N[upos="AUX"] } commands { N.upos=VERB } }
"""

json_data="""
{
  "meta": { "sent_id": "fr-ud-dev_00243", "text": "Ils sont sept." },
  "nodes": {
    "0": { "form": "__0__" },
    "1": {
      "Gender": "Masc",
      "Number": "Plur",
      "Person": "3",
      "PronType": "Prs",
      "form": "Ils",
      "lemma": "il",
      "textform": "Ils",
      "upos": "PRON",
      "wordform": "ils"
    },
    "2": {
      "Mood": "Ind",
      "Number": "Plur",
      "Person": "3",
      "Tense": "Pres",
      "VerbForm": "Fin",
      "form": "sont",
      "lemma": "être",
      "textform": "sont",
      "upos": "AUX",
      "wordform": "sont"
    },
    "3": {
      "Number": "Plur",
      "SpaceAfter": "No",
      "form": "huit",
      "lemma": "huit",
      "textform": "huit",
      "upos": "NUM",
      "wordform": "huit"
    },
    "4": {
      "form": ".",
      "lemma": ".",
      "textform": ".",
      "upos": "PUNCT",
      "wordform": "."
    }
  },
  "edges": [
    { "src": "2", "label": "punct", "tar": "4" },
    { "src": "2", "label": { "1": "comp", "2": "pred" }, "tar": "3" },
    { "src": "2", "label": "subj", "tar": "1" },
    { "src": "0", "label": "root", "tar": "2" }
  ],
  "order": [ "0", "1", "2", "3", "4" ]
}
"""
# ==========================================================================================================

print ("=============  grs ============= ")
# 1) send a connect request
response = requests.post(f"{back}/connect")
session_id = json.loads (response.text)["data"]
print (f"session_id= {session_id}")

# 2bis) send a upload_corpus request
temp = tempfile.NamedTemporaryFile(mode="a+", suffix='.grs', prefix='grew_')
temp.write(grs)
temp.seek(0)  # ready to be read 
r = requests.post(f"{back}/upload_grs",
      data = { "session_id": session_id},
      files = { "file": temp }
    )
temp.close()

webbrowser.open(f"{front}?session_id={session_id}")

exit (0)

# ==========================================================================================================

print ("=============  conll  ============= ")
# 1) send a connect request
response = requests.post(f"{back}/connect")
session_id = json.loads (response.text)["data"]
print (f"session_id= {session_id}")

# 2) send a upload_corpus request
with tempfile.NamedTemporaryFile(mode="a+", suffix='.conll', prefix='grew_') as temp:
  temp.write(conll)
  temp.seek(0)  # ready to be read 
  r = requests.post(f"{back}/upload_corpus",
    data = { "session_id": session_id},
    files = { "file": temp }
  ) 

webbrowser.open(f"{front}?session_id={session_id}")

# ==========================================================================================================

print ("=============  json ============= ")
# 1) send a connect request
response = requests.post(f"{back}/connect")
session_id = json.loads (response.text)["data"]
print (f"session_id= {session_id}")

# 2) send a upload_corpus request
temp = tempfile.NamedTemporaryFile(mode="a+", suffix='.json', prefix='grew_')
temp.write(json_data)
temp.seek(0)  # ready to be read 
r = requests.post(f"{back}/upload_corpus",
      data = { "session_id": session_id},
      files = { "file": temp }
    )
temp.close()

webbrowser.open(f"{front}?session_id={session_id}")


# ==========================================================================================================

print ("=============  conll + grs ============= ")
# 1) send a connect request
response = requests.post(f"{back}/connect")
session_id = json.loads (response.text)["data"]
print (f"session_id= {session_id}")

# 2) send a upload_corpus request
temp = tempfile.NamedTemporaryFile(mode="a+", suffix='.conll', prefix='grew_')
temp.write(conll)
temp.seek(0)  # ready to be read 
r = requests.post(f"{back}/upload_corpus",
      data = { "session_id": session_id},
      files = { "file": temp }
    )
temp.close()

# 2bis) send a upload_corpus request
temp = tempfile.NamedTemporaryFile(mode="a+", suffix='.grs', prefix='grew_')
temp.write(grs)
temp.seek(0)  # ready to be read 
r = requests.post(f"{back}/upload_grs",
      data = { "session_id": session_id},
      files = { "file": temp }
    )
temp.close()

webbrowser.open(f"{front}?session_id={session_id}")
